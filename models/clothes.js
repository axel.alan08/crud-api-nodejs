const { Schema, model } = require("mongoose");

const ClothesSchema = Schema(
    {
        type: {
            type: String,
            required: true,
            enum: ["SWEATER", "SHIRT", "JACKET", "PANTS"],
          },
        quantity: {
            type: Number,
            required: true
        },
        price: {
            type: Number,
            required: true
        },
        description : {
            type: String,
            required: true
        }
    },
    {
        timestamps: true
    }
);

module.exports = model("Clothe", ClothesSchema);