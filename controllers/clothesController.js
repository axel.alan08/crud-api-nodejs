const Clothes = require("../models/clothes");

const getAllClothes = async (req, res) => {
    
    const page = Number(req.query.page);
    const limit = Number(req.query.limit);
    const skipIndex = (page - 1) * limit;
    
    try {
        const count = await Clothes.countDocuments();
        const results = await Clothes.find().sort({ _id: 1 }).limit(limit).skip(skipIndex);
    return res.status(200).json({
        code: "OK",
        message: null,
        success: true,
        data: {
            count,
            results
        }
    });
 } catch (err) {
        return res.status(500).json({
            code: "ERR",
            message: err.message,
            success: false,
            data: null
        });
    }
};

const getClothes = async (req, res) => {
    try {
        const clothes = await Clothes.findById(req.params._id).populate("");
        if(!clothes) {
            return res.status(404).json({
                 code: "NOT-FOUND",
                 message: "The clothes that u are looking for doesn't exists on DB",
                 success: false,
                 data: null
            });
    };
    return res.status(200).json({
        code: "OK",
        message: null,
        success: true,
        data: clothes
    });
 } catch (err) {
        return res.status(500).json({
            code: "ERR",
            message: err.message,
            success: false,
            data: null
        });
    }
};

const postClothes = async (req, res) => {
    try {
        const clothes = await Clothes.create(req.body);
        return res.status(201).json({
            code: "OK",
            message: null,
            success: true,
            data: clothes
        });
    } catch (err) {
        return res.status(500).send({
            code: "ERR",
            message: err.message,
            success: false,
            data: null,
         }); 
    }
};

const putClothes = async (req, res) => {
    try {
        const clothes = await Clothes.findOneAndUpdate({ _id: req.params._id }, { ...req.body }, { new: true });
        if(!clothes){
        return res.status(404).json({
           code: "NOT-FOUND",
           message: "The clothes that u are looking for doesn't exists on DB",
           success: false,
           data: null
       });
       };
       return res.status(200).json({
           code:"OK",
           message: null,
           success: true,
           data: clothes
           });
       } catch (err) {
           return res.status(500).json({
               code: "ERR",
               message: err.message,
               success: false,
               data: null
           });
       }
};

const deleteClothes = async (req, res) => {
    try {
        const clothes = await Clothes.deleteOne({_id: req.params._id});
        return res.status(200).json({
        code: "OK",
        message: null,
        success: true,
        data: null
    });
    } catch (err) {
        return res.status(500).json({
            code: "ERR",
            message: err.message,
            success: false,
            data: null
        });
    }
};

module.exports = {
    getAllClothes,
    getClothes,
    postClothes,
    putClothes,
    deleteClothes,
};