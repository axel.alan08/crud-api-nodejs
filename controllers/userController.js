const User = require("../models/user");
const bcryptjs = require("bcryptjs");

const getAllUsers = async (req, res) => {
  const page = Number(req.query.page);
  const limit = Number(req.query.limit);
  const skipIndex = (page - 1) * limit;

  try {
    const count = await User.countDocuments();
    const results = await User.find()
      .sort({ _id: 1 })
      .limit(limit)
      .skip(skipIndex);
    return res.status(200).json({
      code: "OK",
      message: null,
      success: true,
      data: {
        count,
        results,
      },
    });
  } catch (err) {
    return res.status(500).json({
      code: "ERR",
      message: err.message,
      success: false,
      data: null,
    });
  }
};

const getUser = async (req, res) => {
  try {
    const user = await User.findById(req.params._id);
    if (!user) {
      return res.status(404).json({
        code: "NOT-FOUND",
        message: null,
        success: false,
        data: null,
      });
    }
    return res.status(200).json({
      code: "OK",
      message: null,
      success: true,
      data: user,
    });
  } catch (err) {
    return res.status(500).json({
      code: "ERR",
      message: err.message,
      success: false,
      data: null,
    });
  }
};

const putUser = async (req, res) => {
  console.log(req.body.password);
  if (req.body.password) {
    var { password, ...resto } = req.body;
    resto.password = bcryptjs.hashSync(password, 10);
  }
  try {
    const user = await User.findOneAndUpdate(
      { _id: req.params._id },
      { ...resto },
      { new: true }
    );
    if (!user) {
      return res.status(404).json({
        code: "NOT-FOUND",
        message: null,
        success: false,
        data: null,
      });
    }
    return res.status(200).json({
      code: "OK",
      message: null,
      success: true,
      data: user,
    });
  } catch (err) {
    return res.status(500).json({
      code: "ERR",
      message: err.message,
      success: false,
      data: null,
    });
  }
};

const deleteUser = async (req, res) => {
  try {
    const user = await User.deleteOne({ _id: req.params._id });
    return res.status(200).json({
      code: "OK",
      message: null,
      success: true,
      data: null,
    });
  } catch (err) {
    return res.status(500).json({
      code: "ERR",
      message: err.message,
      success: false,
      data: null,
    });
  }
};

module.exports = {
  getAllUsers,
  getUser,
  putUser,
  deleteUser,
};
