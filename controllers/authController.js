const jwt = require('jsonwebtoken');
const bcryptjs = require('bcryptjs');
const User = require('../models/user');

const register = async (req, res) => {

    const { password, ...resto } = req.body;
    resto.password = bcryptjs.hashSync(password, 10);

    try {
        const user = await User.create(resto);
        return res.status(201).json({
            code: "OK",
            message: null,
            success: true,
            data: user
        });
    } catch (err) {
        return res.status(500).send({
            code: "ERROR",
            message: err.message,
            success: false,
            data: null,
         }); 
    }
};

const login = async (req, res) => {
    const { email, password } = req.body;
    try {
    const user = await User.findOne({ email });
    if (!user || !bcryptjs.compareSync(password, user.password)) {
        return res.status(400).json({
            code: "AUTHENTICATION-ERR",
            message: "Invalid email or password",
            success: false,
            data: null
        })
    }
    
const token = jwt.sign({ _id: user._id }, process.env.PRIVATE_KEY, { expiresIn: "1h" });

    return res.status(200).json({
        code: "OK",
        message: null,
        success: true,
        data: {
            user,
            token
        }
    });
 } catch (err) {
        return res.status(500).json({
            code: "ERROR",
            message: err.message,
            success: false,
            data: null
        });
    }
};

 module.exports = {
     login,
     register
 }