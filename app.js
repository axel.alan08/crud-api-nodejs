require("dotenv").config();
const express = require("express");
const morgan = require("morgan");

const userRoutes = require("./routes/user.routes");
const clothesRoutes = require("./routes/clothes.routes");
const authRoutes = require("./routes/auth.routes");
const cors = require("cors");
const { validateToken } = require("./middlewares/validate-auth");
const dbConnection = require("./configs/mongodb");

const app = express();

app.use(
  cors({
    origin: "*",
  })
);

dbConnection();

app.use(validateToken);

app.use(morgan("tiny"));

app.use(express.json());
app.use(express.text());

app.use("/auth", authRoutes);
app.use("/clothes", clothesRoutes);
app.use("/user", userRoutes);

app.listen(process.env.PORT, () => {
  console.log(`Server running at port: ${process.env.PORT}`);
});
