const express = require('express');
const router = express.Router();

const {
    getAllClothes,
    getClothes,
    postClothes,
    putClothes,
    deleteClothes,
} = require ("../controllers/clothesController");

const {
    validateIdParamClothes,
    validatePostClothes,
    validatePutClothes,
} = require("../middlewares/validate-clothes");

const { validateRoles } = require("../middlewares/validate-auth");

router.get('/', getAllClothes);
router.get('/:_id', validateIdParamClothes, getClothes);
router.post('/', [validateRoles("ADMIN", "EMPLOYER"),  validatePostClothes], postClothes);
router.put('/:_id', [validateRoles("ADMIN", "EMPLOYER"),  validatePutClothes, validateIdParamClothes], putClothes);
router.delete('/:_id', [validateRoles("ADMIN", "EMPLOYER"),  validateIdParamClothes], deleteClothes);

module.exports = router;