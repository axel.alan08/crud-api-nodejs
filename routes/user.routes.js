const express = require("express");
const router = express.Router();

const {
  getAllUsers,
  getUser,
  putUser,
  deleteUser,
} = require("../controllers/userController");

const {
  validateIdParamUser,
  validatePutUser,
} = require("../middlewares/validate-user");

//const { validateOwner } = require("../middlewares/validate-auth")
router.get("/", getAllUsers);
router.get("/:_id", validateIdParamUser("ADMIN"), getUser);
router.put("/:_id", validateIdParamUser("ADMIN"), validatePutUser, putUser);
router.delete("/:_id", validateIdParamUser("ADMIN"), deleteUser);

module.exports = router;
