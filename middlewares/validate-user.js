const Joi = require("joi");
const jwt = require('jsonwebtoken');
const User = require('../models/user');

////Valida que el usuario que realiza el PUT sea ADMIN o dueño del token perteneciente al usuario a modificar mediante _id
const validateIdParamUser = (...roles) => {
  return async (req, res, next) => {
      authorization = req.headers.authorization;
      token = authorization.split(" ")[1];
      const { _id } = jwt.verify(token, process.env.PRIVATE_KEY);
      const user = await User.findById({ _id }); 
      const parsedId = user._id.toString();

      if (roles.includes(req.user.role)) {
          return next();
      }
      const schema = Joi.object({
      _id: Joi.string().valid(parsedId),
      }).required();  
  
  try {
      await schema.validateAsync(req.params);
      return next();
  } catch (err) {
      return res.status(401).json({
      code: "VALIDATION-ERROR",
      message: "Restricted access, invalid Id for make this request",
      success: false,
      data: null,
      });
      }
  };
}

const validatePutUser = async (req, res, next) => {
  const authorization = req.headers.authorization;
  const token = authorization.split(" ")[1];
  const { _id } = jwt.verify(token, process.env.PRIVATE_KEY);
  const user = await User.findById({ _id }); 
  console.log(user.role)
  console.log(req.body.password); 

  const adminSchema = Joi.object({
    password: Joi.string(),
    email: Joi.string().email(),
    role: Joi.string().valid("ADMIN", "EMPLOYER", "USER"),
  }).min(1)

  const userSchema = Joi.object({
    password: Joi.string(),
    email: Joi.string().email(),
  }).min(1)
    if(user.role === "ADMIN") {
      try {
        await adminSchema.validateAsync(req.body);
        return next();
      } catch (err) {
        console.log(err);
        return res.status(400).json({
        code: "VALIDATION-ERROR",
        message: err.message,
        success: false,
        data: null,
      });
      }
    } else {
    try {
      await userSchema.validateAsync(req.body);
      return next();
    } catch (err) {
      console.log(err);
      return res.status(400).json({
      code: "VALIDATION-ERROR",
      message: err.message,
      success: false,
      data: null,
    });
    }
  }
};



module.exports = {
  validatePutUser,
  validateIdParamUser,
};
