function myCors(req, res, nxt) {
  res.header("Access-Control-Allow-Origin", "http://localhost:4200");
  res.header("Access-Control-Allow-Methods", "POST,DELETE,GET,PUT,OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Access-Control-Allow-Origin, Content-Type, Accept, Accept-Language, Origin, User-Agent"
  );
  if (req.method === "OPTIONS") {
    res.sendStatus(204);
  } else {
    next();
  }
}
module.exports = {
  myCors,
};
