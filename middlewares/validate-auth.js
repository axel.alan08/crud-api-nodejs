const jwt = require("jsonwebtoken");
const Joi = require("joi");
const User = require("../models/user");

const validateToken = async (req, res, next) => {
  if (req.url === "/auth/register" || req.url === "/auth/login") return next();

  const authorization = req.headers.authorization;

  if (!authorization) {
    return res.status(401).json({
      code: "AUTHENTICATION-ERROR",
      message: "Token authorization must be provided",
      success: false,
      data: null,
    });
  }
  try {
    const token = authorization.split(" ")[1];

    const { _id } = jwt.verify(token, process.env.PRIVATE_KEY);

    const user = await User.findById({ _id });
    if (!user) {
      return res.status(401).json({
        code: "AUTHENTICATION-ERROR",
        message: "Your session user does not exist anymore",
        success: false,
        data: null,
      });
    }

    req.user = user;

    return next();
  } catch (err) {
    console.log(err);
    return res.status(400).json({
      code: "AUTHENTICATION-ERROR",
      message: err.message,
      success: false,
      data: null,
    });
  }
};

const validateLogin = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().required(),
    password: Joi.string().required(),
  });

  try {
    await schema.validateAsync(req.body);
    return next();
  } catch (err) {
    console.log(err);
    return res.status(400).json({
      code: "VALIDATION-ERROR",
      message: err.message,
      success: false,
      data: null,
    });
  }
};

const validateRoles = (...roles) => {
  return async (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      return res.status(403).json({
        code: "AUTHENTICATION-ERROR",
        message: "Restricted access",
        success: false,
        data: null,
      });
    }
    return next();
  };
};

const validateRegister = async (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string().required(),
    surname: Joi.string().required(),
    password: Joi.string().required(),
    email: Joi.string().required(),
    role: Joi.string().valid("ADMIN", "EMPLOYER", "USER"),
  });

  try {
    await schema.validateAsync(req.body);
    return next();
  } catch (err) {
    console.log(err);
    return res.status(400).json({
      code: "VALIDATION-ERROR",
      message: "Invalid register values",
      success: false,
      data: null,
    });
  }
};

module.exports = {
  validateToken,
  validateLogin,
  validateRoles,
  validateRegister,
};
