const Joi = require("joi");

const validateIdParamClothes = async (req, res, next) => {
  const schema = Joi.object({
    _id: Joi.string().required(),
  }).required();  

try {
  await schema.validateAsync(req.params);
  return next();
} catch (err) {
  console.log(err);
  return res.status(400).json({
    code: "VALIDATION-ERROR",
    message: err.message,
    success: false,
    data: null,
  });
  }
};

const validatePostClothes = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string().valid("SWEATER", "SHIRT", "JACKET", "PANTS").required(),
    quantity: Joi.number().required(),
    price: Joi.number().required(),
    description: Joi.string().required()
  });

 
  try {
    await schema.validateAsync(req.body);
    return next();
  } catch (err) {
    console.log(err);
    return res.status(400).json({
    code: "VALIDATION-ERROR",
    message: err.message,
    success: false,
    data: null,
  });
  }
};

const validatePutClothes = async (req, res, next) => {
  const schema = Joi.object({
    quantity: Joi.number(),
    price: Joi.number(),
    description: Joi.string()
  })

  try {
    await schema.validateAsync(req.body);
    return next();
  } catch (err) {
    console.log(err);
    return res.status(400).json({
    code: "VALIDATION-ERROR",
    message: err.message,
    success: false,
    data: null,
  });
  }
};


module.exports = {
  validatePutClothes,
  validatePostClothes,
  validateIdParamClothes,
};